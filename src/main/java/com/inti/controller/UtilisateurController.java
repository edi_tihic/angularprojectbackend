package com.inti.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inti.entity.Utilisateur;
import com.inti.service.interfaces.IUtilisateurService;

@CrossOrigin
@RestController
public class UtilisateurController {
	
	@Autowired
	IUtilisateurService iutilisateurService;
	
	@RequestMapping(value="users", method = RequestMethod.GET)
	public List<Utilisateur> findAll(){
		return iutilisateurService.findAll();
	}
	
	
	//@RequestParam("idUser") => localhost:9090/users?idUser=5
	//@RequestParam("idUser") Long id, @RequestParam("nom") String nom => localhost:9090/users?idUser=5&nom=Ayari
	//@PathVariable("idUser") => localhost:9090/users/5
	//@PathVariable("idUser")Long id , @PathVariable("nom") String nom => localhost:9090/users/5/Ayari
	@RequestMapping(value="users/{idUtilisateur}", method = RequestMethod.GET)
	public Utilisateur findOne(@PathVariable("idUtilisateur") Long id) {
		return iutilisateurService.findOne(id);
	}
	
	
	@RequestMapping(value = "users", method=RequestMethod.POST)
	public Utilisateur saveUtilisateur(@RequestBody Utilisateur user) {
		return iutilisateurService.save(user);
	}
	
	
	@RequestMapping(value="users/{idUtilisateur}", method = RequestMethod.DELETE)
	public void deleteUtilisateur(@PathVariable("idUtilisateur") Long id) {
		iutilisateurService.delete(id);
	}
	
	@RequestMapping(value = "users/{idUtilisateur}", method=RequestMethod.PUT)
	public Utilisateur updateUtilisateur(@PathVariable("idUtilisateur")Long id,@RequestBody Utilisateur user) {
		Utilisateur currentUser = iutilisateurService.findOne(id);
		currentUser.setNomUtilisateur(user.getNomUtilisateur());
		currentUser.setPrenomUtilisateur(user.getPrenomUtilisateur());
		currentUser.setUsername(user.getUsername());
		currentUser.setPassword(user.getPassword());
		currentUser.setSexe(user.getSexe());
		return iutilisateurService.save(currentUser);
	}
}
