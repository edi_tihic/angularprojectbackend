package com.inti.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inti.entity.Produit;
import com.inti.entity.Utilisateur;
import com.inti.service.interfaces.IProduitService;

@CrossOrigin
@RestController
public class ProduitController {
	
	@Autowired
	IProduitService iproduitService;
	
	@RequestMapping(value="products", method = RequestMethod.GET)
	public List<Produit> findAll(){
		return iproduitService.findAll();
	}
	@RequestMapping(value="products/{idProduit}", method = RequestMethod.GET)
	public Produit findOne(@PathVariable("idProduit") Long id) {
		return iproduitService.findOne(id);
	}
	@RequestMapping(value = "products", method=RequestMethod.POST)
	public Produit saveProduit(@RequestBody Produit produit) {
		return iproduitService.save(produit);
	}
	
	@RequestMapping(value = "products/{idProduit}", method=RequestMethod.DELETE)
	public void deleteProduit(@PathVariable("idProduit") Long id) {
		iproduitService.delete(id);
	}
	
	@RequestMapping(value = "products/{idProduit}", method=RequestMethod.PUT)
	public Produit updateProduit(@PathVariable("idProduit")Long id,@RequestBody Produit produit) {
		Produit currentProduit = iproduitService.findOne(id);
		currentProduit.setLibelle(produit.getLibelle());
		currentProduit.setCodeProduit(produit.getCodeProduit());
		return iproduitService.save(currentProduit);
	}
}
