package com.inti.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.inti.entity.Role;
import com.inti.service.interfaces.IRoleService;
import com.inti.service.interfaces.IRoleService;

@CrossOrigin
@RestController
public class RoleController {

	@Autowired
	IRoleService iroleService;

	@RequestMapping(value = "roles", method = RequestMethod.GET)
	public List<Role> findAll() {
		return iroleService.findAll();
	}

	@RequestMapping(value = "roles/{idRole}", method = RequestMethod.GET)
	public Role findOne(Long id) {
		return iroleService.findOne(id);
	}

	@RequestMapping(value = "roles", method = RequestMethod.POST)
	public Role saveRole(Role role) {
		return iroleService.save(role);
	}

	@RequestMapping(value = "roles/{idRole}", method = RequestMethod.DELETE)
	public void deleteRole(Long id) {
		iroleService.delete(id);
	}

	@RequestMapping(value = "roles/{idRole}", method = RequestMethod.PUT)
	public Role updateRole(@PathVariable("idRole") Long id, @RequestBody Role role) {
		Role currentRole = iroleService.findOne(id);
		currentRole.setLibelle(role.getLibelle());
		return iroleService.save(currentRole);
	}
}
