package com.inti.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Produit implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idProduit;

	private String codeProduit;
	private String libelle;
	@ManyToOne(fetch = FetchType.EAGER)
	private Utilisateur utilisateur;

	public Produit() {
		super();
	}

	public Produit(long idProduit, String codeProduit, String libelle, Utilisateur utilisateur) {
		super();
		this.idProduit = idProduit;
		this.codeProduit = codeProduit;
		this.libelle = libelle;
		this.utilisateur = utilisateur;
	}

	public Produit(String codeProduit, String libelle, Utilisateur utilisateur) {
		super();
		this.codeProduit = codeProduit;
		this.libelle = libelle;
		this.utilisateur = utilisateur;
	}

	public long getIdProduit() {
		return idProduit;
	}

	public void setIdProduit(long idProduit) {
		this.idProduit = idProduit;
	}

	public String getCodeProduit() {
		return codeProduit;
	}

	public void setCodeProduit(String codeProduit) {
		this.codeProduit = codeProduit;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

}
