package com.inti.service.interfaces;

import java.util.List;

import com.inti.entity.Utilisateur;

public interface IUtilisateurService {
	List<Utilisateur> findAll();

	Utilisateur findOne(Long id);

	Utilisateur save(Utilisateur user);

	void delete(Long id);
}
