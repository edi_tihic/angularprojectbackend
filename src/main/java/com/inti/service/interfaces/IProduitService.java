package com.inti.service.interfaces;

import java.util.List;

import com.inti.entity.Produit;

public interface IProduitService {
	List<Produit> findAll();

	Produit findOne(Long id);

	Produit save(Produit produit);

	void delete(Long id);
}
