package com.inti.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inti.entity.Produit;
import com.inti.repositories.ProduitRepository;
import com.inti.service.interfaces.IProduitService;

@Service
public class ProduitService implements IProduitService {
	
	@Autowired
	ProduitRepository produitRepository;
	
	@Override
	public List<Produit> findAll() {
		// TODO Auto-generated method stub
		return produitRepository.findAll();
	}

	@Override
	public Produit findOne(Long id) {
		// TODO Auto-generated method stub
		return produitRepository.findOne(id);
	}

	@Override
	public Produit save(Produit produit) {
		// TODO Auto-generated method stub
		return produitRepository.save(produit);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		produitRepository.delete(id);
	}

}
